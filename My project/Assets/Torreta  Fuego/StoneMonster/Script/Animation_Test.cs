﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_Test : MonoBehaviour {

	public const string ATTACK	= "Anim_Attack";
	

	Animation anim;

	void Start () {

		anim = GetComponent<Animation>();
		
	}
	
	

	public void AttackAni (){
		anim.CrossFade (ATTACK);
	}


}
