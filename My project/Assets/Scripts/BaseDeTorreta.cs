using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDeTorreta : MonoBehaviour
{
    public Botones Botones;
    
    private void OnMouseDown()
    {
        if (Botones.torretaElegida != null)
        {
            GestorDeAudio.instancia.ReproducirSonido("POP");
            GameObject torreta = Instantiate(Botones.torretaElegida, transform.position + transform.up + Vector3.up, Quaternion.identity);
            torreta.transform.SetParent(transform, false);
            torreta.transform.position = transform.position;
            torreta.GetComponent<HabilidadAtacar>().torretaBaseActual = gameObject;
            //Botones.LimpiarSeleccion();
            gameObject.GetComponent<Collider>().enabled = false;
            GameManager.Instance.Torretas.Add(torreta);
        }
    
    }
}
