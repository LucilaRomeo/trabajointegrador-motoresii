using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDisparador : HabilidadAtacar
{
    [SerializeField] public GameObject proyectil;
    public Transform EmptyEnemigo;
    private TenerVida vidaFortaleza;
    
    protected override void Start()
    {
        base.Start();
       vidaFortaleza = GameObject.Find("Fortaleza").GetComponent<TenerVida>();
        Damage = 10;
    }

    public override void Atacar()
    {
        if (enfriamientoDisparo <= 0)
        {
            GameObject proyectilEnemigo = Instantiate(proyectil, EmptyEnemigo.position + EmptyEnemigo.forward, EmptyEnemigo.rotation);
            proyectilEnemigo.AddComponent<ProyectilEnemigo>().damage = Damage;
            enfriamientoDisparo = velocidadDisparo;
        }
    }

  
}
