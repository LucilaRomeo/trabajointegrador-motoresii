using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastilloVida : TenerVida
{
    protected override void Morir()
    {
        GameOver();
        base.Morir();
    }

    private void GameOver()
    {
        Debug.Log("PERDISTE");
    }
}
