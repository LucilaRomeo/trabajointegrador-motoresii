using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TenerVidaEnemigo : TenerVida
{
    public Slider BarraEnemigo;
    protected override void Morir()
    {
        if (cantidadVida < 0)
        {
            cantidadVida = 0;
            GameManager.Instance.billetera.sumarDinero(20);
            GameManager.Instance.RemoverEnemigo(gameObject);
            Destroy(gameObject);
        }

    }
    protected void Update()
    {
        BarraEnemigo.value = cantidadVida;
    }
}
