using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botones : MonoBehaviour
{
    public GameObject torretaElegida { get; private set; }
    public GameObject Torreta;
    public GameObject TorretaFuego;
    public GameObject TorretaHielo;
    public GameObject TorretaLenta;
    public GameObject TorretaExplosiva;
    public GameObject ImgTorretaDisparadora;
    public GameObject ImgTorretaFuego;
    public GameObject ImgTorretaHielo;
    public GameObject ImgTorretaLenta;
    public GameObject ImgTorretaExplosiva;
    public GameObject ImgBase;
    public bool PonerBase;

    public Texture2D imageTorretaFuego;
    public Texture2D imageTorretaDisparadora;
    public Texture2D imageTorretaHielo;
    public Texture2D imageTorretaLenta;
    public Texture2D imageTorretaExplosiva;
    public Texture2D imageBase;


    public void LimpiarSeleccion()
    {
        torretaElegida = null;
    }

    // Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

    public void CrearTorretaUno()
    {
        if (GameManager.Instance.billetera.Dinero >= 15)
        {
            torretaElegida = Torreta;
            GameManager.Instance.billetera.Dinero -=15;
            PonerBase = false;
            ApagarImagenes();
            ImgTorretaDisparadora.SetActive(true);
            Cursor.SetCursor(imageTorretaDisparadora, new Vector2(imageTorretaDisparadora.width / 2, imageTorretaDisparadora.height / 2), CursorMode.Auto);
        }
    }

    public void CrearTorretaFuego()
    {
        if (GameManager.Instance.billetera.Dinero >= 25)
        { torretaElegida = TorretaFuego;
           GameManager.Instance.billetera.Dinero -= -25;
            PonerBase = false;
            ApagarImagenes();
            ImgTorretaFuego.SetActive(true);
            Cursor.SetCursor(imageTorretaFuego, new Vector2(imageTorretaFuego.width / 2, imageTorretaFuego.height / 2), CursorMode.Auto);
        }
    }

    public void CrearTorretaHielo()
    {
        if (GameManager.Instance.billetera.Dinero >= 30)
        {
            torretaElegida = TorretaHielo;
            GameManager.Instance.billetera.Dinero -= 30;
            PonerBase = false;
            ApagarImagenes();
            ImgTorretaHielo.SetActive(true);
            Cursor.SetCursor(imageTorretaHielo, new Vector2(imageTorretaHielo.width / 2, imageTorretaHielo.height / 2), CursorMode.Auto);
        }
    }

    public void CrearTorretaLenta()
    {
        if (GameManager.Instance.billetera.Dinero >= 40)
        {
            torretaElegida = TorretaLenta;
            GameManager.Instance.billetera.Dinero -= 40;
            PonerBase = false;
            ApagarImagenes();
            ImgTorretaLenta.SetActive(true);
            Cursor.SetCursor(imageTorretaLenta, new Vector2(imageTorretaLenta.width / 2, imageTorretaLenta.height / 2), CursorMode.Auto);
        }
    }
    public void CrearTorretaExplosiva()
    {
        if (GameManager.Instance.billetera.Dinero >= 60)
        {
            torretaElegida = TorretaExplosiva;
            GameManager.Instance.billetera.Dinero -= 60;
            PonerBase = false;
            ApagarImagenes();
            ImgTorretaExplosiva.SetActive(true);
            Cursor.SetCursor(imageTorretaExplosiva, new Vector2(imageTorretaExplosiva.width / 2, imageTorretaExplosiva.height / 2), CursorMode.Auto);
        }
    }
    public void ponerBase()
    {
        PonerBase = true;
        LimpiarSeleccion();
        ApagarImagenes();
        ImgBase.SetActive(true);
        Cursor.SetCursor(imageBase, new Vector2(imageBase.width / 2, imageBase.height / 2), CursorMode.Auto);
    }

    public void ApagarImagenes()
    {
        ImgTorretaDisparadora.SetActive(false);
        ImgBase.SetActive(false);
        ImgTorretaFuego.SetActive(false);
        ImgTorretaExplosiva.SetActive(false);
        ImgTorretaLenta.SetActive(false);
        ImgTorretaHielo.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.D))
        {
            GameManager.Instance.billetera.Dinero += 1000;
        }
    }
}
