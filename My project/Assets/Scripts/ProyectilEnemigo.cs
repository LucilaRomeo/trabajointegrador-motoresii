using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilEnemigo : MonoBehaviour
{
    public int damage;
    public void OnTriggerEnter(Collider other)
    {
      
        if (other.gameObject.CompareTag("Fortaleza"))
        {
            other.GetComponent<TenerVida>().RecibirDamage(damage);
            Destroy(gameObject);
        }
    }
}
