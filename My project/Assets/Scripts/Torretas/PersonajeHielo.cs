using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajeHielo : HabilidadAtacar
{
    public Animation Anim_Attack;
    public GameObject Hielo;

    protected override void Start()
    {
        base.Start();
        Hielo.SetActive(false);
        Damage = 20;
    }

    public override void Atacar()
    {
       enemigoActual.GetComponent<TenerVida>().RecibirDamage(Damage);    
        Hielo.SetActive(!Hielo.activeInHierarchy);
        enemigoActual.GetComponent<ControlEnemigo>().velocidad = 0f;
        StartCoroutine(descongelar());
        Anim_Attack.Play("Attack");
        enfriamientoDisparo = velocidadDisparo;
    }
  
    IEnumerator descongelar()
    {
        yield return new WaitForSeconds(7f);
        enemigoActual.GetComponent<ControlEnemigo>().velocidad = 5f;
    }

}
