using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorretaLenta : HabilidadAtacar
{
    public GameObject HechizoLento;
    protected override void Start()
    {
        base.Start();
        HechizoLento.SetActive(false); 
    }
    public override void Atacar()
    {
        enemigoActual.GetComponent<ControlEnemigo>().velocidad = 2f;
        HechizoLento.SetActive(!HechizoLento.activeInHierarchy);
        enfriamientoDisparo = velocidadDisparo;
        StartCoroutine(AumentoVelocidad());
       
    }
  
    IEnumerator AumentoVelocidad()
    {
        yield return new WaitForSeconds(7f);
        enemigoActual.GetComponent<ControlEnemigo>().velocidad = 5f;  
    }
}
