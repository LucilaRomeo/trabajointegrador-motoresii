using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueFuego : HabilidadAtacar
{
    public Animation Anim_Attack;
    
   protected override void Start()
    {
       
        base.Start();
        Damage = 20;
        fuego.SetActive(false);
       
    
    }

    public override void Atacar()
    {
        enemigoActual.GetComponent<TenerVida>().RecibirDamage(Damage);
        Anim_Attack.Play("Anim_Attack");
        fuego.SetActive(!fuego.activeInHierarchy);
       
        enfriamientoDisparo = velocidadDisparo;
        
    }
}
