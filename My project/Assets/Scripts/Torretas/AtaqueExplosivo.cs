using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueExplosivo : HabilidadAtacar
{
    public GameObject Explosion;
    protected override void Start()
    {
        base.Start();
    }

    public override void Atacar()
    {
        torretaBaseActual.GetComponent<Collider>().enabled = true;
        GameObject FuegoA = Instantiate(Explosion, transform.position, Quaternion.identity);
        GameObject FuegoB = Instantiate(Explosion, enemigoActual.transform.position, Quaternion.identity);
        Destroy(enemigoActual);
        Destroy(gameObject);
        Destroy(FuegoA, 3f);
        Destroy(FuegoB, 3f);
    }
}
