using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueDisparo : HabilidadAtacar
{
    [SerializeField] public GameObject proyectil;
    public Transform empty;
    protected override void Start()
    {
        base.Start();
        Damage = 20;
    }

    public override void Atacar()
    {
        GameObject proyectilTorreta = Instantiate(proyectil, empty.position + empty.forward, empty.rotation);
        proyectilTorreta.AddComponent<ProyectilTorreta>().damage = Damage;
        enfriamientoDisparo = velocidadDisparo;
    }
       

   

}
