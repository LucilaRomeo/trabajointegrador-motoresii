using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerHud : MonoBehaviour
{
    public GameObject Perder;
    public Text TextDinero;
    void Start()
    {
      
        Perder.SetActive(false);
        TextDinero.gameObject.SetActive(true);

    }
    private void Update()
    {
        TextDinero.text = GameManager.Instance.billetera.Dinero.ToString();   
    }

}
