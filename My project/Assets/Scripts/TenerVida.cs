using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TenerVida : MonoBehaviour
{
    public Slider BarraVida;
     public int cantidadVida;
    public GameObject perdisteCanvas;
    private void Start()
    {     
        
    }
    public void RecibirDamage(int damage)
    {
        cantidadVida -= damage;

        if (cantidadVida <= 0)
            Morir();
    }

    protected virtual void Morir()
    {
       perdisteCanvas.SetActive(true);
    }
    private void Update()
    {
        BarraVida.value = cantidadVida;
    }
}
