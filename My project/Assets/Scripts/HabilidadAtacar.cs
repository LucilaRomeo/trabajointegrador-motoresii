using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilidadAtacar : MonoBehaviour
{
    public GameObject fuego;
    public float enfriamientoDisparo;
    public float velocidadDisparo = 3f;
    public GameObject enemigoActual;
    [SerializeField] float Rango;
    public bool EsTorreta;
    private List<GameObject> objetivos;
    public GameObject torretaBaseActual;

    protected int Damage;

    protected virtual void Start()
    {
        if (EsTorreta)
        {
            objetivos = GameManager.Instance.Enemigos;
        }
        else
        {
            objetivos = GameManager.Instance.Torretas;
        }
        
        InvokeRepeating("ActualizarEnemigo", 0.0f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {

        if (enemigoActual != null)
        {
            Vector3 posicionObjetivo = new Vector3(enemigoActual.transform.position.x, transform.position.y, enemigoActual.transform.position.z);
            transform.LookAt(posicionObjetivo);
        }

        if (enfriamientoDisparo > 0)
        {
            enfriamientoDisparo -= Time.deltaTime;
        }
    }

    void ActualizarEnemigo()
    {
        float distanciaMasCorta = Mathf.Infinity;
        GameObject enemigoMasCercano = null;

        for(int i = 0; i < objetivos.Count; i++)
        { 
            if(objetivos[i] == null)
            {
                continue;
            }
            float distanciaAEnemigo = Vector3.Distance(transform.position, objetivos[i].transform.position);
            if (distanciaAEnemigo < distanciaMasCorta)
            {
                distanciaMasCorta = distanciaAEnemigo;
                enemigoMasCercano = objetivos[i];
            }

            if (enemigoMasCercano != null && distanciaMasCorta <= Rango)
            {
                enemigoActual = enemigoMasCercano;
                if (enfriamientoDisparo <= 0)
                {
                    Atacar();
                }
            }
            else
            {
                enemigoActual = null;
            }
        }
    }
    public virtual void Atacar()
    {
        Debug.Log("Ataque");
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Rango);
    }

}
