using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilTorreta : MonoBehaviour
{
    public int damage;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            other.GetComponent<TenerVida>().RecibirDamage(damage);
            Destroy(gameObject);
        }
    }
}
