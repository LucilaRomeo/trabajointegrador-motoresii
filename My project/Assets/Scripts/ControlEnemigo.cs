using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlEnemigo : MonoBehaviour
{
    public GameObject fortaleza;
    int currentPosition = 0;
    private List<Transform> Positions;
    public bool moverse = true;
    public bool EnemigoDeRango;
    public float velocidad = 5f;

    protected void Start()
    {
        
      
    }

    public void SetPositions(List<Transform> posicionesObjetivo)
    {
        Positions = new List<Transform>(posicionesObjetivo);
    }

    private void Update()
    {
        if (Positions != null && Positions.Count > 0)
        {
            if (currentPosition == Positions.Count ||
              (EnemigoDeRango && Vector3.Distance(gameObject.transform.position,
              Positions[Positions.Count - 1].position) < 60))
            {
                GetComponent<HabilidadAtacar>().Atacar();
                Debug.Log("Ataco");
            }
            else if (currentPosition < Positions.Count)
            {
                float distanciaFortaleza = Vector3.Distance(gameObject.transform.position, Positions[currentPosition].position);
                Vector3 desiredPosition = new Vector3(Positions[currentPosition].position.x, transform.position.y, Positions[currentPosition].position.z);

                transform.LookAt(desiredPosition);

                if (moverse)
                    transform.position = Vector3.MoveTowards(transform.position, desiredPosition, velocidad * Time.deltaTime);

                if (transform.position == desiredPosition)
                {
                    currentPosition++;
                }
            }
        }
    }

    public List<Transform> getPositions()
    {
        return Positions.GetRange(currentPosition, Positions.Count - currentPosition);
    }
  
}
