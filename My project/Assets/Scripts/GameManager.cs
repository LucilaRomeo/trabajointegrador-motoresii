using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public GameObject Jugador;
    public List<GameObject> Enemigos;
    public List<GameObject> Torretas;
    public Billetera billetera;
    public int PlataInicial;

    void Awake()
    {
        if (Instance != null)
        {
            Destroy(Instance);
            Instance = this;
        }
        else
        {
            Instance = this;

        }
        //DontDestroyOnLoad(this.gameObject);
        billetera = new Billetera(PlataInicial);

        Enemigos = new List<GameObject>();

        Torretas = new List<GameObject>();
    }

    public void AddEnemigo(GameObject enemigo)
    {
        Enemigos.Add(enemigo);
    }

    public void RemoverEnemigo(GameObject enemigo)
    {
        Enemigos.Remove(enemigo);
    }
}
