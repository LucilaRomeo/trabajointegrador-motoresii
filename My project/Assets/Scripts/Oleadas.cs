using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oleadas : MonoBehaviour
{
    public float contador;
    public Olas[] olas;
    private int currentOla = 0;
    private Olas olaActual;
    private bool olaSpawneada = true;
    public GameObject GanasteCanvas;
    [SerializeField] private List<Transform> Positions;

    void Update()
    {
        if (contador > 0 && olaSpawneada) 
            contador -= Time.deltaTime;

        if (contador <= 0 && currentOla < olas.Length)
        {
            contador = olas[currentOla].tiempoAlaSiguienteOla;
            olaActual = olas[currentOla];
            StartCoroutine(SpawnearOlas());
            currentOla++;
            olaSpawneada=false;
        }else if (currentOla >= olas.Length && GameManager.Instance.Enemigos.Count==0)
        {
            GanasteCanvas.SetActive(true);
        }
    }
    private IEnumerator SpawnearOlas()
    {
        for (int i = 0; i <olaActual.enemies.Length; i++)
        {
            GameObject EnemigoGo = Instantiate(olaActual.enemies[i], transform.position,Quaternion.identity).gameObject;
            EnemigoGo.GetComponent<ControlEnemigo>().SetPositions(Positions);
            GameManager.Instance.AddEnemigo(EnemigoGo);
            yield return new WaitForSeconds(olaActual.tiempoEntreEnemigos);            
        }
        olaSpawneada = true;
    }
}


[System.Serializable]
public class Olas
{
    public ControlEnemigo[] enemies;
    public float tiempoEntreEnemigos;
    public float tiempoAlaSiguienteOla;
}
