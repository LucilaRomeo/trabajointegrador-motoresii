 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoMultiplicador : TenerVidaEnemigo
{
   
    public GameObject enemigo;
    public GameObject enemigo2;
    protected override void Morir()
    {
        if (cantidadVida < 0)
        {
            cantidadVida = 0;
            GameManager.Instance.billetera.sumarDinero(20);
            GameManager.Instance.RemoverEnemigo(gameObject);

            GameObject MiniEnemigo = Instantiate(enemigo, transform.position, Quaternion.identity);
            GameObject MiniEnemigo2 = Instantiate(enemigo2, transform.position + transform.forward*5 , Quaternion.identity);
            MiniEnemigo.GetComponent<ControlEnemigo>().SetPositions(GetComponent<ControlEnemigo>().getPositions());
            MiniEnemigo2.GetComponent<ControlEnemigo>().SetPositions(GetComponent<ControlEnemigo>().getPositions());

            GameManager.Instance.AddEnemigo(MiniEnemigo);
            GameManager.Instance.AddEnemigo(MiniEnemigo2);
            Destroy(gameObject);
        }
    }

 



}
