using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueEnemigo : HabilidadAtacar
{
    private TenerVida vidaObjetivo;
    protected override void Start()
    {
        base.Start();
        Damage = 15;
        vidaObjetivo = GameObject.Find("Fortaleza").GetComponent<TenerVida>();
        

    }
    public override void Atacar()
    {
        if (enfriamientoDisparo <= 0)
        {
            vidaObjetivo.RecibirDamage(Damage);
            enfriamientoDisparo = velocidadDisparo;
        }
    }
   

}
