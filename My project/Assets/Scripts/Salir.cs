using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Salir : MonoBehaviour
{
    public void salir()
    {
        Application.Quit();
        Debug.Log("salio");
    }

    public void reintentar(string escena)
    {
        SceneManager.LoadScene(escena);
    }
}
