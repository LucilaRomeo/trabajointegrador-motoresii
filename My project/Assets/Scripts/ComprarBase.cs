using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class ComprarBase : MonoBehaviour
{
    public GameObject BaseTorreta;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
     
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            float mousePosY = Input.mousePosition.y;
            float posicionValida = (Screen.height / 100f) * 17f;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
              Botones botones = GameObject.Find("Botones").GetComponent<Botones>();

                if (mousePosY > posicionValida && GameManager.Instance.billetera.Dinero >= 15 && hit.rigidbody!=null && hit.rigidbody.gameObject.CompareTag("Piso") && botones.PonerBase)
                {
                    //botones.PonerBase = false;
                   GameObject Base = Instantiate(BaseTorreta, hit.point,Quaternion.identity);
                    Base.GetComponent<BaseDeTorreta>().Botones = botones;
                    GameManager.Instance.billetera.Dinero -= 15;

                }

            }
        }
    }
}
